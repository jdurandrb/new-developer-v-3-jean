# Use the official Python 3 base image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the requirements.txt file and install the dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Set up the virtual environment
RUN python3 -m venv venv

# Activate the virtual environment
RUN . venv/bin/activate

# Copy the project files to the working directory
COPY . .

# Expose the port on which the application will run
EXPOSE 8000

# Run the application
CMD ["python3", "index.py"]
