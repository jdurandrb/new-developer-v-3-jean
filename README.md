# NEW DEVELOPER V3 REABAJATUSCUENTAS TEST

## Directory Structure
```
new_developer_v3/
├── RTFM_LMGTFY # Answer 1
├── requirements.txt
├── README.md
├── pipeline.sh # Answer 2
├ # index.py: Answer 4, also handles 1 and 5. Maybe 3 (Uses projects/smtp-gateway to send mail)
├── index.py
├── Dockerfile
├── docker-compose.yml
├── .gitignore
├── .env_example
├── utils/
│   └── ...
├── tests/ 
│   └── ...
├── static/ # Answer 5
│   └── ...
└── projects/ # Answer 3
    └── ...
```


## Set up and run
Make sure you have installed docker-compose and docker on your system
```bash
./pipeline.sh
```

## PS
I almost add unit test the smtp-gateway but for time reasons I didn't.
So at meantime the tests were made only for python project.

The python project doesn't have linting (for above mentioned reason) but the projects modyo-memory and simple-calculator have