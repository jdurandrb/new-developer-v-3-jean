
___

Before LMGT always RTFM. If both fails, ask for help, don't waste your time. I always try to find the solutions by myself, but at the same time I try to not waste a lot of time doing that, if after a exhaustive research I don't find a solutions for some important issue and I have the possibility to discuss this with coworkers, I'll do that to get any other ideas.


I'm using debian since 2010 but I've also used Windows before that and for two years I was using macos for a particular client (almost the same thing as linux in terms of manageability).


I am quite confident in using python, js, ts, C and bash, but I also have experience using php and batch.
Additionally, I have knowledge of C#, Java, Lua and Swift.

___