import os
import re

from dotenv import load_dotenv

from utils.index import (
    find_matching_pair_pointers,
    find_matching_pair_bruteforce,
    print_file_content,
    decode_base64_from_file,
    decode_hex_from_file,
    get_matching_pair_input,
    send_mail
)

load_dotenv()

MAIL_API = os.environ.get('MAIL_API')

def main():
    while True:
        # print menu
        print()
        print("👾                                                  👾")
        print("👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➀  ⋗ Matching Pairs With Brute Force [O(n^2)]    👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➁  ⋗ Matching Pairs With Pointers [O(n log n)]   👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➂  ⋗ Print Answer 1                              👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➃  ⋗ Decode Message                              👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➄  ⋗ Send Email                                  👾\n👾\t\t\t\t\t\t    👾")
        print("👾 ➅  ⋗ Exit                                        👾\n👾\t\t\t\t\t\t    👾")
        print("👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾👾")
        print("👾                                                  👾")
        print()
        # get choice
        choice = input("Enter your choice => ")

        if choice == "1": # Use nested loops to get matching pairs
            numbers, target_sum = get_matching_pair_input()
            print(find_matching_pair_bruteforce(numbers, target_sum))
        elif choice == "2": # Use pointers to get matching pairs
            numbers, target_sum = get_matching_pair_input()
            print(find_matching_pair_pointers(numbers, target_sum))
        elif choice == "3": # Print Answer 1
            print_file_content('RTFM_LMGTFY.md')
        elif choice == "4": # Decode Messages
            print(decode_base64_from_file('static/Answer_6.base64'))
            print(decode_hex_from_file('static/Answer_6.hex'))
        elif choice == "5": # Send Email
            recipient = input("Enter recipient's email address: ")
            if re.match(r'^[\w\.-]+@[\w\.-]+\.\w+$', recipient) is not None:
                content = input("Enter email content: ")
                send_mail(recipient, content, MAIL_API)
            else:
                print("Invalid email address.")
        elif choice == "6": # Exit
            print("Exiting the program...")
            break
        else: # Invalid option
            print('Invalid option')

if __name__ == "__main__":
    main()
