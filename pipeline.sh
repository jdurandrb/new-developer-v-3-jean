#!/bin/bash
set -e

# Define variables
DOCKER_COMPOSE_FILE="docker-compose.yml"

# Set the project name
export COMPOSE_PROJECT_NAME="new_developer"

# Function to log messages
log() {
    echo "[LOG] $(date +"%Y-%m-%d %H:%M:%S") $1"
}

# Build and run the Docker containers
log "Building and running the Docker containers..."
docker-compose -f $DOCKER_COMPOSE_FILE up -d --build

# Run tests
log "Running tests..."
docker-compose run --rm tests

# If tests pass, deploy to production
if [ $? -eq 0 ]; then
    log "Tests passed. Deploying to production..."

    # Stop the containers
    log "Stopping the containers..."
    docker-compose -f $DOCKER_COMPOSE_FILE down --remove-orphans

    # Pull the latest changes from the repository
    log "Pulling the latest changes from the repository..."
    git pull origin main

    # Build and run the Docker containers again
    log "Building and running the Docker containers again..."
    docker-compose -f $DOCKER_COMPOSE_FILE up -d --build

    # Restart any necessary services
    log "Restarting necessary services..."
    docker-compose -f $DOCKER_COMPOSE_FILE restart
    docker run -it $COMPOSE_PROJECT_NAME-app
    log "Deployment successful!"
else
    log "Tests failed. Deployment aborted."
fi
