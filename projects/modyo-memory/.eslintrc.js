module.exports = {
  root: true,
  env: {
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  plugins: ['prettier'],
  extends: [
    'eslint:recommended',
    'prettier',
    'plugin:vue/essential',
  ],
  rules: {
    /* 'import/no-unresolved': ['error', { commonjs: true }], */
    'import/no-extraneous-dependencies': 'error',
    'node/no-missing-require': 'off',
    'node/no-extraneous-import': 'off',
    'no-unused-vars': 'off',
    'class-methods-use-this': 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    /* 'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off', */
    'func-names': 'off',
    'no-process-exit': 'off',
    'no-plusplus': 'off',
    'consistent-return': 'off',
    'no-restricted-globals': 'off',
    'max-len': 'off',
    'no-undef': 'off',
    'semi': 'off',
    'vue/no-unused-components': [
      'warn',
      {
        ignoreWhenBindingPresent: true,
      },
    ],
    camelcase: 'off'
  },
};
