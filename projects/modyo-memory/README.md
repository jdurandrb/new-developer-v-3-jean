# MODYO MEMORY GAME
---

#### Download and install environment
```bash
npm install
```

#### Dev
```bash
npm run dev
```

#### Prod
```bash
npm run build
npm start
```

#### With Docker
```bash
npm run docker:build
npm run docker:run
```

#### Cypress to test solution - It'll take a while
```bash
npm run cypress
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.
