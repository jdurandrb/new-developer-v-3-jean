const { defineConfig } = require("cypress");

module.exports = defineConfig({
  /* experimentalComponentTesting: true, */
  fixtures: false,

  video: false,
  fixturesFolder: false,


  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
