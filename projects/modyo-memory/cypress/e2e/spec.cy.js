describe('test memory game', () => {
  it('should try to get user data or require it', () => {
    const TotalCards = 20
    const cardItem ='body > div > div > div > div.card-body > div.card-content.card-item.no-gutters'
    const saveNameButton = 'body > div > div > div > form > div > div.col-12.center-content.mb-1.text-center > button'
    const urlGetCards = 'https://fed-team.modyo.cloud/api/content/spaces/animals/types/game/entries'
    const mainUrl = 'http://localhost:8080'

    const setName = () => {
      /* click to test error message with empty input */
      cy.get(saveNameButton).click()
      cy.wait(700)
      cy.get('#full-name').type('Jean Durand')
      cy.get(saveNameButton).click()
    }

    /* check if every card has fixed-match class-status */
    const checkTotalMatched = async () => {
      let allMatched = false
      await cy
        .get(cardItem)
        .find('.fixed-match')
        .should('satisfy', ($el) => {
          if ($el.length === (TotalCards * 2)) allMatched = true
          else allMatched = false
          return true
        })
      await cy
        .get(cardItem)
        .find('.qmark')
        .should('satisfy', ($el) => {
          if ($el.length > 0) allMatched = false
          return true
        })
      return allMatched;
    }
    const solveGame = async () => {
      const banned = []
      let matched = false
      /* banned.includes() check if the card to be clicked already matches */
      for (let i = 1; i < (TotalCards * 2); i++) {
        if (banned.includes(i)) continue
        for (let j = i + 1; j < (TotalCards * 2) + 1; j++) {
          if (banned.includes(j)) continue
          /* click two cards and wait */
          await cy.get(`${cardItem} > li:nth-child(${i})`).click()
          await cy.get(`${cardItem} > li:nth-child(${j})`).click()
          cy.wait(2100)
          /*
            then, if there's a fixed-match status-class in the last clicked
            card, then set match to true and checkTotalMatched.
            i and j will be re-setted with new values to continue solving puzzle
          */
          await cy
            .get(`${cardItem} > li:nth-child(${j}) > img`)
            .should('satisfy', ($el) => {
              const classList = Array.from($el[0].classList)
              if (classList.includes('fixed-match')) {
                banned.push(i, j)
                j = (TotalCards * 2)+1
                i = 1
                matched = true
              }
              return true
            })
          if (matched) {
            matched = false
            if (await checkTotalMatched()) return
            break
          }
        }
      }
    }
    cy.visit(mainUrl)
    cy.wait(200)
    cy.request('GET', `${urlGetCards}?per_page=${TotalCards}`).then(() => {
      setName()
      cy.wait(500)
      /* remove score marker to prevent it from covering the cards */
      cy.get('#draggable-container').then(($el) => $el.remove())
      solveGame()
    })
  })
})
