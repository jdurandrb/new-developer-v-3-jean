import { createApp } from 'vue'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'

import MemoryApp from '@/MemoryApp';

createApp(MemoryApp).mount('body')
