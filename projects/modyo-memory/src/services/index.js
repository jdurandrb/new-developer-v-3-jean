import axios from 'axios'

class _GameService {
  getCards(perPage = 20) {
    return new Promise((resolve, reject) => {
      const config = {
        method: 'get',
        url: `${process.env.VUE_APP_URL_GET_CARDS}?per_page=${perPage}`,
        headers: {
          'Content-Type': 'application/json',
          Accept: '*/*',
        },
      }
      axios(config)
        .then((res) => {
          if (res?.status === 200 && res?.data) {
            resolve(res.data)
          }
          reject({ payload: res, message: 'No hay datos para mostrar' })
        })
        .catch(reject)
    })
  }
}

export const GameService = new _GameService()
