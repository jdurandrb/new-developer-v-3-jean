import { uuid } from 'vue-uuid'; 

import { GameService } from '@/services'
export const getCards = async () => {
  return new Promise((resolve) => {
    GameService.getCards().then(data => {
      /* create new object with entries */
      data = data.entries.map((entry) => {
        return {
          id: entry.fields.image.uuid,
          name: entry.meta.name,
          image: entry.fields.image.url,
          /* matched status for animation - temporary*/
          matched: false,
          display: false,
          wrong: false,
          /* matched status for logic control - fixed */
          currMatched: false,
        }
      })
      /* duplicate cards using new UUID to ensure uniqueness */
      data.forEach((elem) =>
        data.push({
          ...elem,
          id: uuid.v4()
        })
      )
      /* shuffle cards */
      data.sort(() => Math.random() - 0.5)
      resolve(data) 
    }).catch((e)=> {
      /* if there's an error, return the message or the default static message */
      resolve(e?.message ??  'No hay datos para mostrar')
    })
  })
}
