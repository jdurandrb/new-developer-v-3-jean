const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  /* transpileDependencies: false, */
  configureWebpack: {
    devtool: 'source-map',
  },
  chainWebpack: (config) => config.optimization.minimize(false),
  assetsDir: 'assets',
  /* runtimeCompiler: true, */
  lintOnSave: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: 
          `@import "@/../public/styles/main.scss";`
      }
    }
  }
});
