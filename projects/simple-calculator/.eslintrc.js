module.exports = {
  root: true,
  env: {
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  plugins: ['prettier'],
  extends: [
    'eslint:recommended',
    'prettier',
  ],
  rules: {
    /* 'import/no-unresolved': ['error', { commonjs: true }], */
    /* 'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off', */
    'func-names': 'off',
    'no-process-exit': 'off',
    'no-plusplus': 'off',
    'consistent-return': 'off',
    'no-restricted-globals': 'off',
    'max-len': 'off',
    'no-undef': 'off',
    'semi': 'off',
    camelcase: 'off'
  },
};
