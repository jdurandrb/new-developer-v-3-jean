# Soleit Simple Calculator

#### Download and install environment
```bash
npm install
```

#### Dev
```bash
npm run dev
```

#### Prod
```bash
npm run build
npm start
```

#### With Docker
```bash
docker build -t soleit-calculator .
docker run --rm -p 3000:3000 soleit-calculator
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
