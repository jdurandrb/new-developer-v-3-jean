"use client";

import React, { useState, useEffect } from "react";
import { ClockIcon } from "@heroicons/react/solid";
import { useCalc } from "../../context/useCalc";
import ChuckNorrisImage from "../../../public/norris.png";
import historyStyles from "../history/history.module.scss";
import styles from "./calculator.module.scss";

const Calculator: React.FC = () => {
  const {
    history,
    result,
    setKeys,
    keys,
    setCalcResult,
    setHistoryEntry,
    setModal,
  } = useCalc();
  let [currentHistoryEntry, setCurrentHistoryEntry] = useState("");
  let [key, setKey] = useState({} as KeyboardEvent);
  let [activeKeyTmp, setActiveKeyTmp] = useState("");

  const isOperator = (value: string) => {
    return ["/", "*", "+", "-", "%", "^", "ALTGRAPH"].includes(
      value?.toUpperCase()
    );
  };

  const isValidInput = (value: string) => {
    return [
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      ".",
      ",",
      "/",
      "*",
      "+",
      "-",
      "%",
      "^",
      "ALTGRAPH",
      "A",
      "C",
      "AC",
      "=",
      "ENTER",
      "BACKSPACE",
    ].includes(value?.toUpperCase())
      ? true
      : false;
  };

  const doNotAllowZeroesAtStart = () => {
    if (result.length > 0 && result.charAt(0) === "0")
      setCalcResult(result.slice(1, result.length));
  };

  const handleClick = (event: InnerMouseEvent, keyEvent?: KeyboardEvent) => {
    let currKey = keyEvent?.key ?? event?.target?.innerText;
    keyEvent ? "" : event.preventDefault();
    if (!isValidInput(currKey)) return;
    updateActiveKeyTmp(currKey);
  };


  const handleInput = (prevKey: string, actKey: string) => {
    if (isOperator(actKey) && isOperator(prevKey)) return;
    if (
      ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", ","].includes(
        actKey
      )
    ) {
      setCalcResult(result + actKey);
    } else if (
      actKey?.toUpperCase() === "BACKSPACE" ||
      (actKey?.toUpperCase() === "C" && result !== "")
    ) {
      let tempResult = result?.toString().slice(0, -1);
      setCalcResult(tempResult);
    } else if (isOperator(actKey?.toString()) && result !== "") {
      setCalcResult(result + actKey.replace("AltGraph", "^"));
    } else if ((result && actKey === "=") || actKey === "Enter") {
      let tempres = eval(result?.toString()?.replace("^", "**"));
      if (tempres === 0 || tempres) {
        setCurrentHistoryEntry(result);
        setCalcResult(tempres);
      }
    } else if (actKey?.toUpperCase() === "A" || actKey === "AC") {
      setCalcResult("");
      setHistoryEntry("");
    }
  };

  const detectKeyDown = (e: KeyboardEvent) => {
    e.preventDefault();
    setKey(e);
  };

  const getLastHistoryEntry = () => {
    const entry = history[history.length - 1];
    return entry?.slice(0, entry.indexOf("= ")) ?? "";
  };

  const updateActiveKeyTmp = (value: string) => {
    setActiveKeyTmp(value);
    setKeys(keys.activeKey, value);
    setTimeout(() => {
      setActiveKeyTmp("");
    }, 200);
  };

  const isActiveKeyTmp = (key: string | number) => {
    if (
      activeKeyTmp?.toString().toUpperCase() === key?.toString().toUpperCase()
    )
      return true;
  };

  useEffect(() => {
    document.addEventListener("keydown", detectKeyDown, true);
  }, []);

  useEffect(() => {
    doNotAllowZeroesAtStart();
  }, [result]);

  useEffect(() => {
    handleInput(keys.previousKey, keys.activeKey);
  }, [keys]);

  useEffect(() => {
    handleClick({} as InnerMouseEvent, key);
  }, [key]);

  useEffect(() => {
    currentHistoryEntry
      ? setHistoryEntry(
          currentHistoryEntry?.toString() + " = " + result?.toString()
        )
      : "";
  }, [currentHistoryEntry]);

  return (
    <>
      <div className={styles.calcWrapper}>
        <div className={styles.allItems + " " + styles.mainBackground}>
          <div
            style={{ padding: "0px 20px 20px 20px" }}
            className={styles.inputBtn + " " + "overflow-y-scroll"}
          >
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "1fr 1fr",
                width: "100%",
              }}
              className={styles.alignSelfTop}
            >
              <div>
                <div>
                  <img
                    className={styles.chuckImage}
                    src={ChuckNorrisImage.src}
                    alt="ChuckNorris"
                    onClick={() => setModal({ type: "norris", value: true })}
                  />
                </div>
              </div>
              <ClockIcon
                style={{ justifySelf: "end" }}
                className={
                  styles.historyIcon +
                  " " +
                  styles.alignSelfTop +
                  " " +
                  "h-5 w-5"
                }
                onClick={() => setModal({ type: "history", value: true })}
              />
            </div>
            <div className={"break-all"}>{getLastHistoryEntry()}</div>
            <div className={historyStyles.result + " " + "break-all"}>
              {result || 0}
            </div>
          </div>
          <div
            style={{ padding: "20px 20px 0px 20px" }}
            className={styles.gridContainer}
          >
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("AC") || isActiveKeyTmp("A")
                  ? styles.pressed
                  : "" +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="AC"
            >
              AC
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("C") || isActiveKeyTmp("BACKSPACE")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="C"
            >
              C
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("+")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="+"
            >
              +
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("-")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="-"
            >
              -
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("%")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="%"
            >
              %
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp(".")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="."
            >
              .
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("1")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="1"
            >
              1
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("2")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="2"
            >
              2
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("AltGraph") || isActiveKeyTmp("^")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="^"
            >
              ^
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("3")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="3"
            >
              3
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("4")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="4"
            >
              4
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("5")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="5"
            >
              5
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("/")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.operations +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="/"
            >
              /
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("6")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="6"
            >
              6
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("7")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="7"
            >
              7
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("8")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="8"
            >
              8
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("*")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont +
                    " " +
                    styles.operations
              }
              name="*"
            >
              *
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("9")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="9"
            >
              9
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("0")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont
              }
              name="0"
            >
              0
            </button>
            <button
              type="button"
              onClick={handleClick}
              className={
                isActiveKeyTmp("=") || isActiveKeyTmp("ENTER")
                  ? styles.pressed
                  : styles.notPressed +
                    " " +
                    styles.gridItem +
                    " " +
                    styles.smallFont +
                    " " +
                    styles.operations
              }
              name="="
            >
              =
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Calculator;
