"use client";

import React, { useState, useEffect } from "react";
import { XCircleIcon } from "@heroicons/react/solid";
import { useCalc } from "../../context/useCalc";
import calcStyles from "../calculator/calculator.module.scss";
import historyStyles from "../history/history.module.scss";
import { getJoke } from "@/app/services/index";
import styles from "./chuck.module.scss";

const Chuck: React.FC = () => {
  const { setModal, chuckModal } = useCalc();
  let [joke, setJoke] = useState("");

  useEffect(() => {
    if (chuckModal) {
      getJoke("https://api.chucknorris.io/jokes/random").then((joke) =>
        setJoke(joke?.value)
      );
    }
  }, [chuckModal]);

  return (
    <>
      <div
        id="modal_overlay_norris"
        className={historyStyles.modalOverlay + " " + "hidden"}
      >
        <div
          id="modal_norris"
          className={
            historyStyles.mainModal +
            " " +
            "opacity-0 transform -translate-y-full transition-opacity transition-transform duration-300"
          }
        >
          <div className={calcStyles.calcWrapper}>
            <div className={calcStyles.allItems}>
              <div
                className={
                  historyStyles.inputBtn + " " + calcStyles.mainBackground
                }
              >
                <div
                  className={
                    calcStyles.alignSelfTop + " " + historyStyles.closeIcon
                  }
                >
                  <XCircleIcon
                    style={{ justifySelf: "start" }}
                    className={
                      calcStyles.alignSelfTop +
                      " " +
                      "h-5 w-5" +
                      " " +
                      calcStyles.historyIcon
                    }
                    onClick={() => setModal({ type: "norris", value: false })}
                  />
                </div>
                <div className={styles.chuckJoke + " " + styles.chuckJokeTitle}>
                  Chuck Norris
                </div>
                <div className={styles.chuckJoke}>{joke}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Chuck;
