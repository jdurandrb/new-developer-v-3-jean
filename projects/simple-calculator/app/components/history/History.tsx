"use client";

import React, { useState, useEffect } from "react";
import { useCalc } from "../../context/useCalc";
import { XCircleIcon, TrashIcon } from "@heroicons/react/solid";
import calcStyles from "../calculator/calculator.module.scss";
import styles from "./history.module.scss";

const History: React.FC = () => {
  const { history, deleteHistoryEntry, setModal } = useCalc();
  let [historyNodes, setHistoryNodes] = useState([] as React.JSX.Element[]);
  const updateHistoryNodes = () => {
    const rows = [];
    for (let i = 0; i < history.length; i++) {
      rows.push(
        <div
          key={i}
          className={styles.historyItem}
          style={{ minHeight: "75px" }}
        >
          <div className={styles.entry}>
            <div className={styles.operation}>
              {history[i].split(" = ")[0]}
              <div
                onClick={() => {
                  deleteHistoryEntry(history[i]);
                }}
              >
                <TrashIcon className={styles.trashIcon + " " + "h-5 w-5"} />
              </div>
            </div>
            <div className={styles.result}>{history[i].split(" = ")[1]}</div>
          </div>
        </div>
      );
    }
    setHistoryNodes(rows);
  };

  useEffect(() => {
    updateHistoryNodes();
  }, [history]);

  return (
    <>
      <div
        id="modal_overlay_history"
        className={styles.modalOverlay + " " + "hidden"}
      >
        <div
          id="modal_history"
          className={
            styles.mainModal +
            " " +
            "opacity-0 transform -translate-y-full transition-opacity transition-transform duration-300"
          }
        >
          <div className={calcStyles.calcWrapper}>
            <div className={calcStyles.allItems}>
              <div
                className={styles.inputBtn + " " + calcStyles.mainBackground}
              >
                <div
                  className={calcStyles.alignSelfTop + " " + styles.closeIcon}
                >
                  <XCircleIcon
                    style={{ justifySelf: "start" }}
                    className={
                      calcStyles.alignSelfTop +
                      " " +
                      "h-5 w-5" +
                      " " +
                      calcStyles.historyIcon
                    }
                    onClick={() => setModal({ type: "history", value: false })}
                  />
                </div>
                <div className={styles.historyTitle}>History</div>
                {!historyNodes || historyNodes?.length === 0 ? (
                  <p
                    className={styles.operation}
                    style={{
                      textTransform: "uppercase",
                      fontWeight: "600",
                      fontSize: "10px",
                    }}
                  >
                    No operations registered
                  </p>
                ) : (
                  historyNodes
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default History;
