import { createContext, useState } from "react";

interface CalcContextValue {
  result: string;
  history: string[];
  chuckModal: boolean;
  keys: Keys;
  setKeys: (previousKey: string, activeKey: string) => void;
  setCalcResult: (result: string) => void;
  setHistoryEntry: (result: string) => void;
  deleteHistoryEntry: (historyEntry: string) => void;
  setModal: (modal: CalcModals) => void;
}

export const CalcContext = createContext<CalcContextValue>({
  keys: { previousKey: "", activeKey: "" },
  result: "",
  history: [],
  chuckModal: false,
  setKeys: () => {
    throw new Error("setKeys() not implemented");
  },
  setCalcResult: () => {
    throw new Error("setCalcResult() not implemented");
  },
  setHistoryEntry: () => {
    throw new Error("setHistory() not implemented");
  },
  deleteHistoryEntry: () => {
    throw new Error("deleteHistoryEntry() not implemented");
  },
  setModal: () => {
    throw new Error("setModal() not implemented");
  },
});

interface Props {
  children: React.ReactNode;
}

export const CalcProvider: React.FC<Props> = ({ children }) => {
  const [keys, setBothKeys] = useState<Keys>({} as Keys);
  const [result, setResult] = useState<string>("");
  const [history, setHistory] = useState<string[]>([]);
  const [chuckModal, setChuckModal] = useState<boolean>(false);

  const setKeys = (previousKey: string, activeKey: string) => {
    setBothKeys({ previousKey, activeKey });
  };
  const setCalcResult = (result: string) => {
    setResult(result);
  };
  const setHistoryEntry = (result: string) => {
    if (result) setHistory([...history, result]);
  };
  const deleteHistoryEntry = (result: string) => {
    const tmpHistory = [...history];
    tmpHistory.splice(history.indexOf(result), 1);
    setHistory(tmpHistory);
  };
  const setModal = (modal: CalcModals) => {
    if (modal.type === "norris") setChuckModal(modal.value);
    const modal_overlay = document.querySelector(
      `#modal_overlay_${modal.type}`
    );
    const _modal = document.querySelector(`#modal_${modal.type}`);
    const modalCl = _modal?.classList;
    const overlayCl = modal_overlay;
    if (modal.value) {
      overlayCl?.classList.remove("hidden");
      setTimeout(() => {
        modalCl?.remove("opacity-0");
        modalCl?.remove("-translate-y-full");
        modalCl?.remove("scale-150");
      }, 100);
    } else {
      modalCl?.add("-translate-y-full");
      setTimeout(() => {
        modalCl?.add("opacity-0");
        modalCl?.add("scale-150");
      }, 100);
      setTimeout(() => overlayCl?.classList.add("hidden"), 300);
    }
  };

  return (
    <>
      <CalcContext.Provider
        value={{
          result,
          history,
          chuckModal,
          keys,
          setKeys,
          setCalcResult,
          setHistoryEntry,
          deleteHistoryEntry,
          setModal,
        }}
      >
        {children}
      </CalcContext.Provider>
    </>
  );
};
