import { useContext } from "react";
import { CalcContext } from "./CalcContext";

export const useCalc = () => {
  const context = useContext(CalcContext);
  if (!context) throw new Error("useCalc must be used within a CalcProvider");
  return context;
};
