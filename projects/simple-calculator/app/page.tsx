"use client";
import { CalcProvider } from "./context/CalcContext";
import Calculator from "./components/calculator/Calculator";
import History from "./components/history/History";
import Chuck from "./components/chuck/Chuck";

export default function Home() {
  return (
    <main>
      <CalcProvider>
        <Calculator />
        <History />
        <Chuck />
      </CalcProvider>
    </main>
  );
}
