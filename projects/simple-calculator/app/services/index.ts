import { fetcher } from "@/app/utils/fetcher";

export const getJoke = async (uri: string): Promise<any> => {
  return await fetcher(`${uri.replace(/\/$/g, "")}`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
};
