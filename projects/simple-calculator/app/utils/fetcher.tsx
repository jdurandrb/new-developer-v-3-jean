export const fetcher = async (
  resource: RequestInfo,
  init?: RequestInit
): Promise<any> => {
  const res = await fetch(resource, init);
  if (!res.ok) {
    const error = new Error("Error fetching data");
    const info = await res.json();
    (error as any).status = res.status;
    throw error;
  }
  return await res.json();
};
