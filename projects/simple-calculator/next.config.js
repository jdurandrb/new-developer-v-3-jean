const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
  PHASE_TEST
} = require('next/constants')

const getBuildConfig = () => {
  const path = require('path')

  const cssOptions = {
    sassOptions: {
      includePaths: [path.join(process.cwd(), 'src', 'common', 'css')],
    },
  }

  const nextConfig = {
    ...cssOptions,
    webpack(config) {
      return config
    },
    output: 'standalone',
  }
  return nextConfig
}

module.exports = (phase) => {
  const shouldAddBuildConfig =
    phase === PHASE_DEVELOPMENT_SERVER || phase === PHASE_PRODUCTION_BUILD || phase === PHASE_TEST
  return shouldAddBuildConfig ? getBuildConfig() : {}
}
