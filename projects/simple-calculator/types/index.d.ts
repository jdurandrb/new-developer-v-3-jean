/* EVENTS */
interface InnerTextEventTarget extends EventTarget {
  innerText: string;
}
interface InnerMouseEvent extends React.MouseEvent<HTMLButtonElement> {
  target: InnerTextEventTarget;
}

/* GLOBAlS */
interface CalcModals {
  value: boolean;
  type: string;
}
type Keys = {
  previousKey: string;
  activeKey: string;
}
/* HERO ICONS */
declare module "@heroicons/react/solid";

/* API */
