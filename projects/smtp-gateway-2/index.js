const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
require("dotenv").config();
const config = require("./src/config").ENV;
global.ENV = config;
global.node_env = process.env.NODE_ENV;

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT; // env variable
    this.apiPath = process.env.API_PATH; // env variable
    this.middlewares(); // Middlewares
    this.routes(); // Routes
  }

  routes() {
    this.app.use(this.apiPath, require("./src/routes/index"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`[♛] Ready on port ${this.port} [♛]`);
    });
  }

  middlewares() {
    this.app.use(cors()); // CORS
    this.app.use(helmet()); // security
    this.app.use(express.urlencoded({ extended: false })); // parse application/json
    this.app.use(express.json()); // parses incoming requests with JSON payloads and is based on body-parser
  }

  get() {
    return this.app;
  }
}

const server = new Server();

server.listen();

module.exports = server.get();