# SMTP MAIL GATEWAY

#### Download and install environment
```bash
npm install
```

#### Dev
```bash
npm run dev
```

#### Prod
```bash
npm start
```

#### With Docker
Build and Run
```bash
npm run docker-setup
```
Or into separate steps
```bash
npm run docker-build
npm run docker-run
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
