// MAIL CONFIG
const nodemailer = require("nodemailer");

const mainService = require('../services')

const transporter = nodemailer.createTransport({
  host: process.env.smtp_host,
  port: 587,
  secure: false,
  auth: {
    user: process.env.email,
    pass: process.env.password,
  },
});

// Check server health
const health = () => {
  return (req, res, next) => {
    mainService
      .health()
      .then((r) => {
        res.status(200).json({ message: "OK" });
      })
      .catch(console.log);
  };
};

//send mail
const sendMail = async (req, res) => {
  new Promise(async (resolve, reject) => {
    const mailOptions = {
      from: process.env.email, // sender address
      to: req.body.email, // list of receivers
      subject: 'Solución', // Subject line
      html: `<h1> Mensaje:  </h1><br /><p> ${req.body.message} </p>`, // plain text body
    };
    await transporter.sendMail(mailOptions, function (err, info) {
      if (err) {
        console.log(err);
        reject(err);
      }
      else {
        res.json({
          message: `Email enviado a ${req.body.email}`,
        });
        resolve(info)
      }
    });
  })
};

module.exports = {
  health,
  sendMail,
};
