const { Router } = require("express");
const router = Router();

const { health, sendMail } = require("../controllers/index");

// check server health
router.get("/", health())

// send mail
router.post("/send", sendMail)

module.exports = router;
