from io import StringIO
import requests
import unittest
import base64
import binascii
import os
import tempfile

from dotenv import load_dotenv
from unittest.mock import patch

from utils.index import (
    find_matching_pair_pointers,
    find_matching_pair_bruteforce,
    print_file_content,
    decode_base64_from_file,
    decode_hex_from_file,
    send_mail
)

load_dotenv()

MAIL_API = os.environ.get('MAIL_API')

class MainTests(unittest.TestCase):

    def setUp(self):
        self.temp_file = tempfile.NamedTemporaryFile(delete=False)
        self.temp_file.write(b"Line 1\nLine 2\nLine 3\nLine 4\nLine 5\nLine 6\nLine 7\nLine 8\nLine 9\nLine 10\n")
        self.temp_file.close()

    def test_file_with_content_and_at_least_10_lines(self):
        expected_output = "Line 4\nLine 7\nLine 10"
        actual_output = print_file_content(self.temp_file.name)
        self.assertEqual(actual_output, expected_output)

    def test_file_with_content_and_less_than_10_lines(self):
        expected_output = ''
        actual_output = print_file_content('path/to/invalid/file')
        self.assertEqual(actual_output, expected_output)

    def test_file_does_not_exist(self):
        expected_output = ''
        actual_output = print_file_content('path/to/nonexistent/file')
        self.assertEqual(actual_output, expected_output)

    def tearDown(self):
        os.remove(self.temp_file.name)

    @patch('requests.post')
    def test_send_email_success(self, mock_post):
        # Mock the response from the API
        mock_post.return_value.status_code = 200

        # Call the send_email function with test data
        email = "jl.durand1995@gmail.com"
        message = "This is a test email"
        send_mail(email, message, MAIL_API)

        # Assert that the API was called with the correct payload
        mock_post.assert_called_once_with(
            f"{MAIL_API}/send",
            json={"email": email, "message": message}
        )

        # Assert that the success message is printed
        self.assertEqual(
            mock_post.return_value.status_code,
            200,
            "Email sent successfully!"
        )

    @patch('requests.post')
    def test_send_email_failure(self, mock_post):
        # Mock the response from the API
        mock_post.return_value.status_code = 500

        # Call the send_email function with test data
        email = "jl.durand1995@gmail.com"
        message = "This is a test email"
        send_mail(email, message, MAIL_API)

        # Assert that the API was called with the correct payload
        mock_post.assert_called_once_with(
            f"{MAIL_API}/send",
            json={"email": email, "message": message}
        )

        # Assert that the failure message is printed
        self.assertEqual(
            mock_post.return_value.status_code,
            500,
            "Failed to send email."
        )

    def test_decode_base64_from_file(self):
        # Create a temporary file with base64 encoded data
        file_path = 'test_file.txt'
        encoded_data = base64.b64encode(b'new-developer_v3').decode('utf-8')
        with open(file_path, 'w') as file:
            file.write(encoded_data)

        # Call the function and assert the decoded data
        decoded_data = decode_base64_from_file(file_path)
        self.assertEqual(decoded_data, 'new-developer_v3')
        os.remove(file_path)

    def test_decode_hex_from_file(self):
        # Create a temporary file with hex encoded data
        file_path = 'test_file.txt'
        encoded_data = binascii.hexlify(b'new-developer_v3').decode('utf-8')
        with open(file_path, 'w') as file:
            file.write(encoded_data)

        # Call the function and assert the decoded data
        decoded_data = decode_hex_from_file(file_path)
        self.assertEqual(decoded_data, 'new-developer_v3')
        os.remove(file_path)

    def get_matching_pair_input(self):
        while True:
            try:
                arr = input('Enter numbers separated by spaces like this 1 2 3 4 5 => ')
                if not arr:
                    raise ValueError("No numbers entered.")
                numbers = list(map(int, arr.split(' ')))
                if not numbers:
                    raise ValueError("No valid numbers entered.")
                target_sum = int(input('Enter Target Sum => '))
                print()
                return numbers, target_sum
            except ValueError as e:
                print(f"Invalid input: {e}")

    def test_find_matching_pair_pointers(self):
        # Existing test case
        numbers = [1, 2, 3, 4, 5]
        target_sum = 7
        expected_result = (2, 5)
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

        # Test with an empty list
        numbers = []
        target_sum = 7
        expected_result = None
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

        # Test with a list containing only one number
        numbers = [1]
        target_sum = 7
        expected_result = None
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

        # Test with a list containing negative numbers
        numbers = [-1, -2, 3, 4, 5]
        target_sum = 2
        expected_result = (-2, 4)
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

        # Test with a target sum that is not possible to achieve
        numbers = [1, 2, 3, 4, 5]
        target_sum = 10
        expected_result = None
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

        # Test with an unordered list
        numbers = [5, 4, 3, 2, 1]
        target_sum = 7
        expected_result = (2, 5)
        self.assertEqual(find_matching_pair_pointers(numbers, target_sum), expected_result)

    def test_find_matching_pair_bruteforce(self):
        # Existing test case
        numbers = [1, 2, 3, 4, 5]
        target_sum = 7
        expected_result = (2, 5)
        self.assertEqual(find_matching_pair_bruteforce(numbers, target_sum), expected_result)

        # Test with an empty list
        numbers = []
        target_sum = 7
        expected_result = None
        self.assertEqual(find_matching_pair_bruteforce(numbers, target_sum), expected_result)

        # Test with a list containing only one number
        numbers = [1]
        target_sum = 7
        expected_result = None
        self.assertEqual(find_matching_pair_bruteforce(numbers, target_sum), expected_result)

        # Test with a list containing negative numbers
        numbers = [-1, -2, 3, 4, 5]
        target_sum = 2
        expected_result1 = (-2, 4)
        expected_result2 = (-1, 3)
        self.assertIn(find_matching_pair_bruteforce(numbers, target_sum), [expected_result1, expected_result2])

        # Test with a target sum that is not possible to achieve
        numbers = [1, 2, 3, 4, 5]
        target_sum = 10
        expected_result = None
        self.assertEqual(find_matching_pair_bruteforce(numbers, target_sum), expected_result)

        # Test with an unordered list
        numbers = [5, 4, 3, 2, 1]
        target_sum = 7
        expected_result1 = (5, 2)
        expected_result2 = (4, 3)
        self.assertIn(find_matching_pair_bruteforce(numbers, target_sum), [expected_result1, expected_result2])


if __name__ == "__main__":
    unittest.main()
