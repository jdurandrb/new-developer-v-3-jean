import requests
import binascii
import base64

def send_mail(recipient, content, mail_api):
    # API endpoint for sending emails
    api_url = f"{mail_api}/send"
    # Prepare the request payload
    payload = {
        "message": content,
        "email": recipient
    }

    # Send the POST request to the API
    response = requests.post(api_url, json=payload)

    # Check the response status code
    if response.status_code == 200:
        print("Email sent successfully!")
    else:
        print("Failed to send email.")


def find_matching_pair_pointers(numbers, target_sum):
    # Sort the numbers list in ascending order
    numbers.sort()
    left = 0
    right = len(numbers) - 1
    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target_sum:
            # Return the pair of numbers that sum up to the target sum
            return (numbers[left], numbers[right])
        elif current_sum < target_sum:
            left += 1  # Move the left pointer to the right
        else:
            right -= 1  # Move the right pointer to the left
    # Return None if no matching pair is found
    return None

def find_matching_pair_bruteforce(numbers, target_sum):
    for i in range(len(numbers)):
        for j in range(i+1, len(numbers)):
            if numbers[i] + numbers[j] == target_sum:
                # Return the pair of numbers that sum up to the target sum
                return (numbers[i], numbers[j])
    # Return None if no matching pair is found
    return None

def print_file_content(file_path):
    try:
        with open(file_path, 'r') as file:
            lines = [line.strip() for line in file]
        print(f"{lines[3]}\n{lines[6]}\n{lines[9]}")
        return f"{lines[3]}\n{lines[6]}\n{lines[9]}"
    except:
        return ''

def decode_base64_from_file(file_path):
    with open(file_path, 'rb') as file:
        encoded_data = file.read()
        decoded_data = base64.b64decode(encoded_data)
        return decoded_data.decode('utf-8')

def decode_hex_from_file(file_path):
    with open(file_path, 'r') as file:
        encoded_data = file.read().strip()
        decoded_data = binascii.unhexlify(encoded_data).decode('latin-1', errors='ignore')
        return decoded_data

def get_matching_pair_input():
    arr = input('Enter numbers separated by spaces like this 1 2 3 4 5 => ')
    numbers = list(map(int,arr.split(' ')))
    target_sum = int(input('Enter Target Sum => '))
    print()
    return numbers, target_sum
